--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   18:45:14 11/05/2020
-- Design Name:   
-- Module Name:   /home/brunno/Projetos/Doutorado/GENERIC_ARCH_DT/tb_top.vhd
-- Project Name:  GENERIC_ARCH_DT
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_unsigned.ALL;
USE work.STD_DT.all;
 
ENTITY tb_top IS
END tb_top;
 
ARCHITECTURE behavior OF tb_top IS 
  
    COMPONENT top
    PORT(
         RST_mem : IN  std_logic;
         RST_core : IN  std_logic;
         CLK : IN  std_logic;
         we : IN  std_logic;
         sel_address : IN  std_logic;
         address_ext : IN  std_logic_vector(BITS_ADDR-1 downto 0);
         datain : IN  std_logic_vector(WIDTH_DATA-1 downto 0);
         decision : OUT  std_logic_vector(NUM_CLASSIF-1 downto 0);
         done : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal RST_mem : std_logic := '0';
   signal RST_core : std_logic := '0';
   signal CLK : std_logic := '0';
   signal we : std_logic := '0';
   signal sel_address : std_logic := '0';
   signal address_ext : std_logic_vector(BITS_ADDR-1 downto 0) := (others => '0');
   signal datain : std_logic_vector(WIDTH_DATA-1 downto 0) := (others => '0');

 	--Outputs
   signal decision : std_logic_vector(NUM_CLASSIF-1 downto 0);
   signal done : std_logic;

   -- Clock period definitions
   constant CLK_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: top PORT MAP (
          RST_mem => RST_mem,
          RST_core => RST_core,
          CLK => CLK,
          we => we,
          sel_address => sel_address,
          address_ext => address_ext,
          datain => datain,
          decision => decision,
          done => done
        );

   -- Clock process definitions
   CLK_process :process
   begin
		CLK <= '0';
		wait for CLK_period/2;
		CLK <= '1';
		wait for CLK_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
		RST_mem <= '1';
		RST_core <= '1';
      wait for CLK_period;
		RST_mem <= '0';
		we <= '1';
		address_ext <= "000";
		datain <= "000000000000000010000000000000000";
		wait for CLK_period;
		we <= '1';
		address_ext <= "001";
		datain <= "000000000011001010000110000000000";
		wait for CLK_period;
		we <= '1';
		address_ext <= "010";
		datain <= "000110000000011010000000000111000";
		wait for CLK_period;
		we <= '1';
		address_ext <= "011";
		datain <= "100010000000000010000000000000000";
		wait for CLK_period;
		we <= '1';
		address_ext <= "100";
		datain <= "100100000000000010000000000000000";
		wait for CLK_period;
		we <= '1';
		address_ext <= "101";
		datain <= "101000000000000010000000000000000";
		wait for CLK_period;
		we <= '1';
		address_ext <= "110";
		datain <= "110000000000000010000000000000000";
		wait for CLK_period;
		we <= '1';
		address_ext <= "111";
		datain <= "111111110000000010000000000000000";
		wait for CLK_period;
		we <= '0';
		sel_address <= '1';
		RST_core <= '0';
      wait;
   end process;

END;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use work.STD_DT.ALL;

entity uc_dt is
	Port(
		RST: in STD_LOGIC;
		CLK: in STD_LOGIC;
		is_leaf: in STD_LOGIC;
		comp_result: in STD_LOGIC;
		address: out STD_LOGIC_VECTOR(BITS_ADDR-1 downto 0);
		load_comp: out STD_LOGIC;
		load_dec: out STD_LOGIC;
		done: out STD_LOGIC
	);
end uc_dt;

architecture Behavioral of uc_dt is

type t_state is (idle, first_batch, calc, s_done);
signal state, next_state: t_state;
signal s_address, reg_current_address: STD_LOGIC_VECTOR(BITS_ADDR-1 downto 0);
signal offset_address: STD_LOGIC_VECTOR(BITS_ADDR-1 downto 0);
signal offset_address_2: STD_LOGIC_VECTOR(BITS_ADDR-1 downto 0);
signal inc_offset: STD_LOGIC;
signal load_addr: STD_LOGIC;
signal aux_address: STD_LOGIC_VECTOR(BITS_ADDR-1 downto 0);
signal cfg_addr: STD_LOGIC_VECTOR(1 downto 0);

begin

process(RST, CLK, load_addr)
begin
	if(RST = '1') then
		reg_current_address <= (OTHERS => '0');
	elsif(CLK'event and CLK='1') then
		if(load_addr = '1') then
			reg_current_address <= s_address;
		end if;
	end if;
end process;

process(RST, CLK, inc_offset)
begin
	if(RST = '1') then
		offset_address <= "010";
	elsif(CLK'event and CLK='1') then
		if(inc_offset = '1') then
			offset_address <= offset_address(BITS_ADDR-2 downto 0) & '0';
		end if;
	end if;
end process;

offset_address_2 <= offset_address + "010";

address <= s_address;

with comp_result select
	aux_address <= reg_current_address + offset_address when '1',
					 reg_current_address + offset_address_2 when OTHERS;

with cfg_addr select
	s_address <= (OTHERS => '0') when "00",
					  (0 => '1', OTHERS => '0') when "01",
					  aux_address when OTHERS;

process(RST, CLK)
begin
	if(RST = '1') then
		state <= idle;
	elsif(CLK'event and CLK='1') then
		state <= next_state;
	end if;
end process;

process(state, is_leaf, load_addr)
begin
	case state is
		when idle =>
			load_dec <= '0';
			cfg_addr <= "00";
			inc_offset <= '0';
			load_addr <= '0';
			load_comp <= '0';
			done <= '0';
			next_state <= first_batch;
			
		when first_batch =>
			load_dec <= '0';
			cfg_addr <= "01";
			inc_offset <= '0';
			load_addr <= '1';
			load_comp <= '0';
			done <= '0';
			next_state <= calc;
			
		when calc =>
			cfg_addr <= "10";
			inc_offset <= '1';
			load_addr <= '1';
			load_comp <= '1';
			done <= '0';
			if(is_leaf = '1') then
				load_dec <= '1';
				next_state <= s_done;
			else
				load_dec <= '0';
				next_state <= calc;
			end if;
			
		when s_done =>
			load_dec <= '0';
			cfg_addr <= "10";
			inc_offset <= '0';
			load_addr <= '0';
			load_comp <= '1';
			done <= '1';
			next_state <= s_done;
			
	end case;
end process;

end Behavioral;


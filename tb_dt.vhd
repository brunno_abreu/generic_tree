LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
ENTITY tb_dt IS
END tb_dt;
 
ARCHITECTURE behavior OF tb_dt IS 
 
    COMPONENT dt
    PORT(
         RST : IN  std_logic;
         CLK : IN  std_logic;
         possible_feat_1 : IN  std_logic_vector(15 downto 0);
         possible_cte_1 : IN  std_logic_vector(15 downto 0);
         possible_feat_2 : IN  std_logic_vector(15 downto 0);
         possible_cte_2 : IN  std_logic_vector(15 downto 0);
         --sel_and : IN  std_logic;
         should_be_true : IN  std_logic;
         classif_value : IN  std_logic_vector(3 downto 0);
         end_of_path : IN  std_logic;
         next_value : OUT  std_logic;
         decision : OUT  std_logic_vector(3 downto 0);
         done : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal RST : std_logic := '0';
   signal CLK : std_logic := '0';
   signal possible_feat_1 : std_logic_vector(15 downto 0) := (others => '0');
   signal possible_cte_1 : std_logic_vector(15 downto 0) := (others => '0');
   signal possible_feat_2 : std_logic_vector(15 downto 0) := (others => '0');
   signal possible_cte_2 : std_logic_vector(15 downto 0) := (others => '0');
   --signal sel_and : std_logic := '0';
   signal should_be_true : std_logic := '0';
   signal classif_value : std_logic_vector(3 downto 0) := "0000";
   signal end_of_path : std_logic := '0';

 	--Outputs
   signal next_value : std_logic;
   signal decision : std_logic_vector(3 downto 0);
   signal done : std_logic;

   -- Clock period definitions
   constant CLK_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: dt PORT MAP (
          RST => RST,
          CLK => CLK,
          possible_feat_1 => possible_feat_1,
          possible_cte_1 => possible_cte_1,
          possible_feat_2 => possible_feat_2,
          possible_cte_2 => possible_cte_2,
          --sel_and => sel_and,
          should_be_true => should_be_true,
          classif_value => classif_value,
          end_of_path => end_of_path,
          next_value => next_value,
          decision => decision,
          done => done
        );

   -- Clock process definitions
   CLK_process :process
   begin
		CLK <= '0';
		wait for CLK_period/2;
		CLK <= '1';
		wait for CLK_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      RST <= '1';
		wait for CLK_period;
		RST <= '0';
		--while(done = '0') loop
		--	possible_feat_1 <= "0010011101010101";
		--	possible_cte_1 <= "0010100101010010";
		--	possible_feat_2 <= "0010011101010101";
		--	possible_cte_2 <= "0010100101010010";
		--	sel_and <= '0';
		--	end_of_path <= '0';
		--	should_be_true <= '1';
		--	classif_value_1 <= "0010";
		--	classif_value_2 <= "0100";
		--	wait for CLK_period;
		--end loop;

		possible_feat_1 <= "1001011101010010";
		possible_cte_1 <= "0101101001110011";
		possible_feat_2 <= "1001011101010010";
		possible_cte_2 <= "0101101001110011";
		--sel_and <= '0';
		end_of_path <= '0';
		should_be_true <= '1';
		classif_value <= "0010";
		wait for CLK_period;
		
		possible_feat_1 <= "0101010111101000";
		possible_cte_1 <= "0001100011101001";
		possible_feat_2 <= "0000000000000000";
		possible_cte_2 <= "0000000000000000";
		--sel_and <= '1';
		end_of_path <= '0';
		should_be_true <= '1';
		classif_value <= "1000";
		wait for CLK_period;
		
		possible_feat_1 <= "0100100100100100";
		possible_cte_1 <= "0001111001111001";
		possible_feat_2 <= "0000000000000000";
		possible_cte_2 <= "0000000000000001";
		--sel_and <= '1';
		end_of_path <= '0';
		should_be_true <= '1';
		classif_value <= "0010";
		wait for CLK_period;
		
		possible_feat_1 <= "0111011010101010";
		possible_cte_1 <= "1000110010101001";
		possible_feat_2 <= "0100100100100100";
		possible_cte_2 <= "1001000001000000";
		--sel_and <= '1';
		end_of_path <= '1';
		should_be_true <= '1';
		classif_value <= "1000";
		wait for CLK_period;

      wait;
   end process;

END;

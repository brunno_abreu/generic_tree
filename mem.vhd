library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.NUMERIC_STD.all;
use work.STD_DT.all;

entity mem is
  port (
	 RST: in STD_LOGIC;
    CLK: in STD_LOGIC;
    we: in STD_LOGIC;
    address: in STD_LOGIC_VECTOR(BITS_ADDR-1 downto 0);
    datain: in STD_LOGIC_VECTOR(WIDTH_DATA-1 downto 0);
    dataout_1: out STD_LOGIC_VECTOR(WIDTH_DATA-1 downto 0);
	 dataout_2: out STD_LOGIC_VECTOR(WIDTH_DATA-1 downto 0)
  );
end mem;

architecture Behavioral of mem is

signal ram: ram_type;
signal read_address: STD_LOGIC_VECTOR(BITS_ADDR-1 downto 0);

begin

process(RST, CLK, we)
begin
	if(RST = '1') then
		for i in 0 to LINES_MEM - 1 loop
			ram(i) <= (OTHERS => '0');
		end loop;
	elsif(CLK'event and CLK='1') then
		if(we = '1') then
			ram(to_integer(unsigned(address))) <= datain;
		end if;
		read_address <= address;
	end if;
end process;

dataout_1 <= ram(to_integer(unsigned(read_address)));
dataout_2 <= ram(to_integer(unsigned(read_address) + "001"));

end Behavioral;
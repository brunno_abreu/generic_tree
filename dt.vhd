library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use work.STD_DT.all;

entity dt is
	Port(
		RST: in STD_LOGIC;
		CLK: in STD_LOGIC;
		possible_feat_1: in STD_LOGIC_VECTOR(WIDTH_FEAT-1 downto 0);
		possible_cte_1: in STD_LOGIC_VECTOR(WIDTH_FEAT-1 downto 0);
		possible_feat_2: in STD_LOGIC_VECTOR(WIDTH_FEAT-1 downto 0);
		possible_cte_2: in STD_LOGIC_VECTOR(WIDTH_FEAT-1 downto 0);
		is_leaf_1: in STD_LOGIC;
		is_leaf_2: in STD_LOGIC;
		classif_value_1: in STD_LOGIC_VECTOR(NUM_CLASSIF-1 downto 0);
		classif_value_2: in STD_LOGIC_VECTOR(NUM_CLASSIF-1 downto 0);
		address: out STD_LOGIC_VECTOR(BITS_ADDR-1 downto 0);
		decision: out STD_LOGIC_VECTOR(NUM_CLASSIF-1 downto 0);
		done: out STD_LOGIC
	);
end dt;

architecture Behavioral of dt is

signal reg_feat_1, reg_feat_2: STD_LOGIC_VECTOR(WIDTH_FEAT-1 downto 0);
signal reg_cte_1, reg_cte_2: STD_LOGIC_VECTOR(WIDTH_FEAT-1 downto 0);
signal reg_classif_value_1, reg_classif_value_2: STD_LOGIC_VECTOR(NUM_CLASSIF-1 downto 0);
signal reg_is_leaf_1, reg_is_leaf_2, reg_data_arrived: STD_LOGIC;

signal mux_feat: STD_LOGIC_VECTOR(WIDTH_FEAT-1 downto 0);
signal mux_cte: STD_LOGIC_VECTOR(WIDTH_FEAT-1 downto 0);
signal mux_is_leaf: STD_LOGIC;
signal mux_classif_value: STD_LOGIC_VECTOR(NUM_CLASSIF-1 downto 0);
signal cmp_1_1: STD_LOGIC;
signal and_out, reg_and: STD_LOGIC;
signal mux_and: STD_LOGIC;
signal mux_cmp: STD_LOGIC;

signal reg_comp: STD_LOGIC;
signal load_comp: STD_LOGIC;

component uc_dt is
	Port(
		RST: in STD_LOGIC;
		CLK: in STD_LOGIC;
		is_leaf: in STD_LOGIC;
		comp_result: in STD_LOGIC;
		address: out STD_LOGIC_VECTOR(BITS_ADDR-1 downto 0);
		load_comp: out STD_LOGIC;
		done: out STD_LOGIC
	);
end component;

begin

	inst_uc: uc_dt
		Port map(
			RST => RST,
			CLK => CLK,
			comp_result => cmp_1_1,
			is_leaf => mux_is_leaf,
			address => address,
			load_comp => load_comp,
			done => done
		);

	process(RST, CLK)
	begin
		if(RST = '1') then
			reg_feat_1 <= (OTHERS => '0');
			reg_cte_1 <= (OTHERS => '0');
			reg_feat_2 <= (OTHERS => '0');
			reg_cte_2 <= (OTHERS => '0');
			reg_classif_value_1 <= (OTHERS => '0');
			reg_classif_value_2 <= (OTHERS => '0');
			reg_is_leaf_1 <= '0';
			reg_is_leaf_2 <= '0';
		elsif(CLK'event and CLK='1') then
			reg_feat_1 <= possible_feat_1;
			reg_cte_1 <= possible_cte_1;
			reg_feat_2 <= possible_feat_2;
			reg_cte_2 <= possible_cte_2;
			reg_classif_value_1 <= classif_value_1;
			reg_classif_value_2 <= classif_value_2;
			reg_is_leaf_1 <= is_leaf_1;
			reg_is_leaf_2 <= is_leaf_2;
		end if;
	end process;
	
	process(RST, CLK, load_comp)
	begin
		if(RST = '1') then
			reg_comp <= '1';
		elsif(CLK'event and CLK = '1') then
			if(load_comp = '1') then
				reg_comp <= cmp_1_1;
			end if;
		end if;
	end process;
	
	process(mux_feat, mux_cte)
	begin
		if(mux_feat < mux_cte) then
			cmp_1_1 <= '1';
		elsif(mux_feat = mux_cte) then
			cmp_1_1 <= '1';
		else
			cmp_1_1 <= '0';
		end if;
	end process;
				  
--with reg_comp select
--	mux_feat <= reg_feat_1 when '1',
--					 reg_feat_2 when OTHERS;
					 
--with reg_comp select
--	mux_cte <= reg_cte_1 when '1',
--				  reg_cte_2 when OTHERS;
				  
--with reg_comp select
--	mux_is_leaf <= reg_is_leaf_1 when '1',
--						reg_is_leaf_2 when OTHERS;
						
--with reg_comp select
--	mux_classif_value <= reg_classif_value_1 when '1',
--								reg_classif_value_2 when OTHERS;

with reg_comp select
	mux_feat <= possible_feat_1 when '1',
					possible_feat_2 when OTHERS;
						
with reg_comp select
	mux_cte <= possible_cte_1 when '1',
				  possible_cte_2 when OTHERS;
				  
with reg_comp select
	mux_is_leaf <= is_leaf_1 when '1',
						is_leaf_2 when OTHERS;
						
with reg_comp select
	mux_classif_value <= classif_value_1 when '1',
								classif_value_2 when OTHERS;
	
decision <= mux_classif_value;

end Behavioral;


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use work.STD_DT.ALL;

entity top is
	Port(
		RST_mem: in STD_LOGIC;
		RST_core: in STD_LOGIC;
		CLK: in STD_LOGIC;
		we: in STD_LOGIC;
		sel_address: in STD_LOGIC;
		address_ext: in STD_LOGIC_VECTOR(BITS_ADDR-1 downto 0);
		datain: in STD_LOGIC_VECTOR(WIDTH_DATA-1 downto 0);
		decision: out STD_LOGIC_VECTOR(NUM_CLASSIF-1 downto 0);
		done: out STD_LOGIC
	);
end top;

architecture Behavioral of top is

component dt is
	Port(
		RST: in STD_LOGIC;
		CLK: in STD_LOGIC;
		possible_feat_1: in STD_LOGIC_VECTOR(WIDTH_FEAT-1 downto 0);
		possible_cte_1: in STD_LOGIC_VECTOR(WIDTH_FEAT-1 downto 0);
		possible_feat_2: in STD_LOGIC_VECTOR(WIDTH_FEAT-1 downto 0);
		possible_cte_2: in STD_LOGIC_VECTOR(WIDTH_FEAT-1 downto 0);
		is_leaf_1: in STD_LOGIC;
		is_leaf_2: in STD_LOGIC;
		classif_value_1: in STD_LOGIC_VECTOR(NUM_CLASSIF-1 downto 0);
		classif_value_2: in STD_LOGIC_VECTOR(NUM_CLASSIF-1 downto 0);
		address: out STD_LOGIC_VECTOR(BITS_ADDR-1 downto 0);
		decision: out STD_LOGIC_VECTOR(NUM_CLASSIF-1 downto 0);
		done: out STD_LOGIC
	);
end component;

component mem is
	Port(
		RST: in STD_LOGIC;
		CLK: in STD_LOGIC;
		we: in STD_LOGIC;
		address: in STD_LOGIC_VECTOR(BITS_ADDR-1 downto 0);
		datain: in STD_LOGIC_VECTOR(WIDTH_DATA-1 downto 0);
		dataout_1: out STD_LOGIC_VECTOR(WIDTH_DATA-1 downto 0);
		dataout_2: out STD_LOGIC_VECTOR(WIDTH_DATA-1 downto 0)
	);
end component;

signal address_dt, s_address: STD_LOGIC_VECTOR(BITS_ADDR-1 downto 0);
signal s_datain: STD_LOGIC_VECTOR(WIDTH_DATA-1 downto 0);
signal s_dataout_1: STD_LOGIC_VECTOR(WIDTH_DATA-1 downto 0);
signal s_dataout_2: STD_LOGIC_VECTOR(WIDTH_DATA-1 downto 0);

begin

inst_dt: dt
	Port map(
		RST => RST_core,
		CLK => CLK,
		possible_feat_1 => s_dataout_1(WIDTH_DATA-2 downto WIDTH_DATA-WIDTH_FEAT-1),
		possible_cte_1 => s_dataout_1(WIDTH_DATA-WIDTH_FEAT-2 downto 0),
		possible_feat_2 => s_dataout_2(WIDTH_DATA-2 downto WIDTH_DATA-WIDTH_FEAT-1),
		possible_cte_2 => s_dataout_2(WIDTH_DATA-WIDTH_FEAT-2 downto 0),
		is_leaf_1 => s_dataout_1(WIDTH_DATA-1),
		is_leaf_2 => s_dataout_2(WIDTH_DATA-1),
		classif_value_1 => s_dataout_1(WIDTH_DATA-2 downto WIDTH_DATA-NUM_CLASSIF-1),
		classif_value_2 => s_dataout_2(WIDTH_DATA-2 downto WIDTH_DATA-NUM_CLASSIF-1),
		address => address_dt,
		decision => decision,
		done => done
	);
	
inst_mem: mem
	Port map(
		RST => RST_mem,
		CLK => CLK,
		we => we,
		address => s_address,
		datain => datain,
		dataout_1 => s_dataout_1,
		dataout_2 => s_dataout_2
	);
	
with sel_address select
	s_address <= address_dt when '1',
					 address_ext when OTHERS;

end Behavioral;